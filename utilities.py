nlp = English()
def split_into_lemmas_spacy(comment) :
    doc = nlp(comment)
    return [w.lemma_ for w in doc]
