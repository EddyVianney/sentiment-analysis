from flask import Flask, request, jsonify, render_template
import pickle
from classes import SingleColumnSelector, TextStats
from joblib import dump, load
import pandas as pd

app = Flask(__name__)

#load the model
model = pickle.load(open('model.pkl', 'rb'))
#model = load('model.joblib')


@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict',methods=['POST'])
def predict_polarity():
    '''
    For rendering results on the interface
    '''
    #retrieve comment from GUI interace
    comment = request.form.values()
    #transform into dataFrame
    prediction = model.predict(pd.dataFrame(comment))
    #send response to the view
    return render_template('index.html', predicted_polarity=prediction)

@app.route('/predict_api',methods=['POST'])
def predict_api():
    '''
    For direct API call (without using the GUI interface)
    '''
    data = request.get_json(force=True)
    prediction = model.predict([np.array(list(data.values()))])

    output = prediction[0]
    return jsonify(output)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
